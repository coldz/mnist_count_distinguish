import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def re_show(model, p3):
    re = model.predict((p3 / 255).reshape((1, 28, 28)))
    print(np.argmax(re))
    # 处理
    plt.imshow(p3, cmap=plt.cm.binary)
    # 显示
    plt.show()


def model_call():
    model = tf.keras.models.load_model('./models/tf_model.h5')
    return model


def image_read():
    path = input('path:')
    im = Image.open(path)
    im = im.resize((28, 28))#调整大小和模型输入大小一致
    p3 = np.array(im)
    print(p3.shape)
    p3 = p3.min(axis=-1)#对图片进行灰度化处理
    plt.imshow(p3, cmap='gray')
    for i in range(28):#将白底黑字变成黑底白字   由于训练模型是这种格式
        for j in range(28):
            p3[i][j] = 255 - p3[i][j]
    return p3


def main():
    model = model_call()
    while True:
        p3 = image_read()
        re_show(model, p3)


if __name__ == '__main__':
    main()
