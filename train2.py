import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
"""
bp神经网络手写数字识别
x_train:训练数据集<class 'numpy.ndarray'>
y_train:训练数据集的结果<class 'numpy.ndarray'>
x_test:测试数据集
y_test:测试数据集的结果
"""


def mnist_load():
    data_path = r'D:\cold\tf2.0.0\mnist\mnist.npz'
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data(data_path)
    x_train = tf.keras.utils.normalize(x_train, axis=1)
    x_test = tf.keras.utils.normalize(x_test, axis=1)
    return [x_train, y_train, x_test, y_test]


def model_create():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))
    return model


def model_train(model, x_train, y_train, x_test, y_test):
    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.fit(x_train, y_train, epochs=50)
    return model


def model_evaluate(model,x_test, y_test):
    val_loss, val_acc = model.evaluate(x_test, y_test)
    print(val_loss)
    print(val_acc)


def test_show(model, x_test):
    for i in range(5):
        # 处理
        plt.imshow(x_test[i], cmap=plt.cm.binary)
        # 显示
        plt.show()
        predictions = model.predict(x_test)
        print(np.argmax(predictions[i]))


def model_save(model):
    model.save('./models/tf_model.h5', save_format='tf')


def main():
    x_train, y_train, x_test, y_test = mnist_load()
    model = model_create()
    model = model_train(model, x_train, y_train, x_test, y_test)
    model_evaluate(model, x_test, y_test)
    model_save(model)
    test_show(model, x_test)


if __name__ == '__main__':
    main()
