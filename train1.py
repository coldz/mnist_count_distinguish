import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
"""
x_train:训练数据集<class 'numpy.ndarray'>
y_train:训练数据集的结果<class 'numpy.ndarray'>
x_test:测试数据集
y_test:测试数据集的结果
var_loss:测试集损失值
var_acc:准确率
"""
#kears数据集本地加载
datapath = r'D:\cold\tf2.0.0\mnist\mnist.npz'
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data(datapath)
#便于处理，将数据映射到1-0区间
x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1)
# print(type(x_train))
# print(type(y_train))
# print(type(x_test))
# print(type(x_test))



#创建一个容器
model = tf.keras.models.Sequential()
#拉直层：tf.keras.layers.Flatten() #拉直层可以变换张量的尺寸，把输入特征拉直为一维数组，是不含计算参数的层
model.add(tf.keras.layers.Flatten())
#bp神经网络--全连接层：tf.keras.layers.Dense(神经元个数， activation = "激活函数“，kernel_regularizer = "正则化方式），其中：activation可选 relu 、softmax、 sigmoid、 tanh等
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
#添加一个输出 128 的全连接层？？129不行？
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
#即 max(features, 0)。将大于0的保持不变，小于0的数置为0。  relu 相比其他激活函数计算简单。---这理由，，，
#Softmax简单的说就是把一个N*1的向量归一化为（0，1）之间的值，由于其中采用指数运算，使得向量中数值较大的量特征更加明显。
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))
#model.compile(optimizer='优化方法',loss='计算损失',  metrics=['列表，包含评估模型在训练和测试时的性能的指标,这里选择精度'])
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
#训练，特征集，然后每完成一次所有 batch 运算就算为 epoch
model.fit(x_train, y_train, epochs=3)

#评估，返回loss accuracy <class 'numpy.float64'>
val_loss, val_acc = model.evaluate(x_test, y_test)
print(val_loss)
print(val_acc)

#i_max = 10000
for i in range(5):
    #处理
    plt.imshow(x_test[i], cmap=plt.cm.binary)
    #显示
    plt.show()
    predictions = model.predict(x_test)
    print(np.argmax(predictions[i]))